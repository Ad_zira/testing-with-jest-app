/**
 * @jest-environment jsdom
 */

import React from 'react'
import {render, screen} from '@testing-library/react'
import Index from '../pages/index'

describe('App', () => {
  it ('renders a heading', () => {
    const {getByRole} = render(<Index />)
  
    // eslint-disable-next-line
    const heading = screen.getByRole('heading', {
      name: /welcome to next\.js!/i,
    })
  
    expect(heading).toBeInTheDocument()
  })
})

test('use jsdom in this test file', () => {
  const element = document.createElement('div');
  expect(element).not.toBeNull();
});