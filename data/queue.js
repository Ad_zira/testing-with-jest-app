/* Queue in JavaScript */

function QueueFactory () {
  const items = [];

  return {
    enqueue,
    dequeue,
    front,
    isEmpty,
    size,
    print,
  }

  function createQueueElement(element, priority) {
    return {
      element, 
      priority
    }
  }

  function enqueue(element, priority) {
    const newElement = createQueueElement(element, priority)
    let added = false;

    for (let index = 0; index < items.length; index++) {
      const currentElement = items[index];

      if (newElement.priority < currentElement.priority) {
        items.splice(index, 0, newElement);
        added = true;
        break; // We don't need to keep running the loop
      }
    }
    
    if (!added) {
      items.push(newElement)
    }
  }

  function dequeue() {
    items.shift();
    // Friendly reminder that shift mutates `items` array.
  }

  function front() {
    return items[0]
  }

  function isEmpty() {
    return items.length === 0;
  }
  
  function size() {
    return items.length;
  }

  function print() {
    for (const item of items) {
      // console.log(items.toString());
      console.log(`element: ${item.element} - priority: ${item.priority}`)
    }
  }
}

const myQueue = QueueFactory();

myQueue.enqueue(3);
myQueue.enqueue(2);
myQueue.enqueue(6);

console.log(myQueue.front()); // 3
myQueue.print(); // 3,2,6

console.log(myQueue.dequeue()); // 3
myQueue.print(); // 2,6

/*  */