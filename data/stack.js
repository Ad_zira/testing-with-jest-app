/* Stack in JavaScript */

function Stack() {
  let items = [];

  function push(element) {
    items.push(element);
  }
  function pop() {
    return items.pop();
  }
  function peek() {
    return items[items.length - 1];
  }
  function isEmpty() {
    return items.length === 0;
  }
  function size() {
    return items.length;
  }
  function clear() {
    items = [];
  }

  return {
    push,
    pop,
    peek,
    isEmpty,
    size,
    clear
  }
}

const stack = Stack();

function sleep(timeout) {
  return new Promise((resolve) => setTimeout(resolve, timeout));
}

async function washPlates(plates) {
  const timeToWashAPlateInMilliseconds = 3000;
  const platesStack = Stack();

  // plates.forEach((plate) => platesStack.push(plate))
  for(const plate of plates) {
    platesStack.push(plate)
  }

  console.log(`🤖 says: I have ${platesStack.size()} plates to wash!`)
  console.log("🤖 says: Starting the duty!");

  while (!platesStack.isEmpty()) {
    // do something
    const currentPlate = platesStack.pop();
    console.log("🤖 says: Start washing plate:", currentPlate)
    await sleep(timeToWashAPlateInMilliseconds);
    console.log(`🤖 says: Plate ${currentPlate} done.`);
  }
  
  console.log("🤖 says: All plates are cleaned!")
  // return 
  
}

const evil = ['corruptor', 'sexual-assaulter', 'terrorist', 'racist', 'separatist(maybe)'];

washPlates(evil);

// Decimal to binary problem using LIFO
function decimalToBinary(decimal) {
  const binaries = Stack(); // a stack which will hold the binary value from each division;

  let binaryResult = '';
  
  let nextNumber = decimal; // will hold the next nubmer we need to divide

  do {
    binaries.push(nextNumber % 2);

    nextNumber = Math.floor(nextNumber / 2)
  } while (nextNumber !== 0);

  while(!binaries.isEmpty()) {
    binaryResult += binaries.pop();
  }

  return binaryResult;
}
